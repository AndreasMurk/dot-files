export XDG_CONFIG_HOME="$HOME/.config"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export PATH=$PATH:"$HOME/.local/bin"
export LOCAL="$HOME/.local/bin"

setopt share_history # share history between sessions
setopt hist_ignore_all_dups # remove old duplicated commands
setopt hist_ignore_space # ignore space prefixed commands
setopt hist_reduce_blanks # remove superfluous blanks

# keep track of switched directories
setopt auto_pushd pushd_ignore_dups

# if command is a dir, cd into it
setopt auto_cd

# dont print pwd when changing dirs (i.e: cd -)
setopt cd_silent

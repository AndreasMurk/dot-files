source_file () { [ -r "$1" ] && source "$1"; }

exists() { command -v "$1" > /dev/null; }

catcopy() {
    if [ -n "$1" ] && [ -f "$1" ]; then
        cat "$1" | xclip -sel clip
    else
        echo "Usage: copy <filename>"
    fi
}

# source .profile
source_file $HOME/.profile

# source .xprofile
source_file $HOME/.xprofile

export ZSH="$XDG_CONFIG_HOME/.oh-my-zsh"
export HISTFILE="$XDG_CONFIG_HOME/.zsh_history"
export UPDATE_ZSH_DAYS=13
# TODO: should be outsourced
export HISTSIZE=1000000000000000
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/gcr/ssh"

# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ ! -f $XDG_CONFIG_HOME/zsh/.p10k.zsh ]] || source_file $XDG_CONFIG_HOME/zsh/.p10k.zsh

ZSH_THEME="powerlevel10k/powerlevel10k"

plugins=(
    git
    git-extras
    copypath
    dircycle
    extract
    fzf
    last-working-dir
    rsync
    sudo
    z
    gem
    bundler
    ruby
    rvm
    sublime
    colorize
    history
    history-substring-search
    compleat
    zsh-autosuggestions
    zsh-completions
    warhol
    kubectl
    docker
    virtualenv
)

# source oh-my-zsh
source_file $ZSH/oh-my-zsh.sh

# Import aliases
[ -f $HOME/.config/aliasrc ] && source_file $HOME/.config/aliasrc

# Initial loading script for zsh-fuzzyfinder
[ -f ~/.fzf.zsh ] && source_file ~/.fzf.zsh

# For hiding user-name
DEFAULTS_USER "$USER" prompt_context(){}

# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        source "$BASE16_SHELL/profile_helper.sh"

source_file "$LOCAL/zsh-theme-switch"

# nvm
source /usr/share/nvm/init-nvm.sh

export FPATH="$(which eza)/completions/zsh:$FPATH"

# check for theme settings
check_theme

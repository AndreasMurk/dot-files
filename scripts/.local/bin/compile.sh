#!/usr/bin/env bash

# compiles based on file extention and shebang
file=$(readlink -f "$1"); shift;
dir=$(dirname "$file")
base="${file%.*}"
cd "$dir" || exit

# make file executable
chmod a+x "$file"

case "$file" in
    *\.c) gcc $CFLAGS -g "$file" -lm -o "$base".exe && "$base".exe "$@";;
    *\.cpp) g++ $CPPFLAGS -g "$file" -lm -o "$base".exe && "$base".exe "$@";;
    *\.tex) tex "$file" ;;
    *\.md) pandoc "$file" --pdf-engine xelatex -o "$base".pdf;;
    *\.py) python "$file" ;;
    *) sed 1q "$file" | grep "^#\!/" | sed "s/^#\!//" | xargs -r -I % "$file" "$@";;
esac


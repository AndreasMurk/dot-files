#!/usr/bin/env bash

IDS=$(xsetwacom --list devices | awk ' { print $8 } ')

for ID in ${IDS[@]}; do
    xsetwacom set ${ID} MapToOutput 2560x1440+0+0
done

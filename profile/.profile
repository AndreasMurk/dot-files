#!/bin/sh
# if running bash
if [ -n "$BASH_VERSION" ]; then
	# include .bashrc if it exists
	if [ -f "$HOME/.bashrc" ]; then
		. "$HOME/.bashrc"
	fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
	PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
	PATH="$HOME/.local/bin:$PATH"
fi

[ -f $HOME/.config/fzfrc ] && source $HOME/.config/fzfrc

# config dirs
export XDG_CONFIG_HOME="$HOME/.config" && mkdir -p ${XDG_CONFIG_HOME}
export XDG_DATA_HOME="$HOME/local/share" && mkdir -p ${XDG_DATA_HOME}
export ZDOTDIR="$XDG_CONFIG_HOME/zsh" && mkdir -p ${ZDOTDIR}
export _Z_DATA="$XDG_DATA_HOME/z/.z" && mkdir -p ${XDG_DATA_HOME}/z

# user defined directories
export DEV="$HOME/Developing" && mkdir -p "$DEV"
export JOAIA="$DEV/joaia" && mkdir -p "$JOAIA"
export PRIVATE="$DEV/private" && mkdir -p "$PRIVATE"
export WEB="$PRIVATE/webdev" && mkdir -p "$WEB"
export CLOUD="$HOME/Nextcloud" && mkdir -p "$CLOUD"
export LOCAL="$HOME/.local/bin" && mkdir -p "$LOCAL"
export SSH_DIR="$HOME/.ssh" && mkdir -p "$SSH_DIR"
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/gcr/ssh"

# for vscode
export GIT_ASKPASS=/usr/lib/seahorse/ssh-askpass
export SSH_ASKPASS=/usr/lib/seahorse/ssh-askpass
export SSH_ASKPASS_REQUIRE=prefer


# compile with make
export CFLAGS='-Wall -Werror -std=gnu11'
export CXXFLAGS='-Wall -Werror -std=c++1z'

# editor
export VISUAL=nvim
export EDITOR=nvim

# browser
export BROWSER=brave

# npm
export PATH="$PATH:$HOME/npm/bin"

# pnpm
export PATH="$PATH:/usr/bin/pnpm"

# jetbrains toolbox
export PATH="$PATH:$HOME/.local/share/JetBrains/Toolbox/scripts"

# gems for tmuxinator
export GEM_PATH="$HOME/local/share/gem/ruby/3.3.0"
export PATH="$GEM_PATH/bin:$PATH"

# colorful man pages
command -v "bat" >/dev/null &&
	export MANPAGER="sh -c 'col -bx | bat -l man -p'"

. "$HOME/local/share/../bin/env"

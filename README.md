Personal configuration files for my Linux environment. Managed with GNU Stow.

## Included Configurations

- **vim** - Text editor configuration
- **zsh** - Shell configuration
- **tmux** - Terminal multiplexer settings
- **ranger** - File manager configuration
- **rofi** - Application launcher setup
- **zathura** - PDF viewer settings
- **xournalpp** - Note-taking app configuration
- **xinit/xprofile** - X11 startup configuration

## Prerequisites

- GNU Stow
- Git

## Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/AndreasMurk/dot-files.git
   cd dot-files
   ```

2. Run the install script:

   ```bash
   ./install.sh
   ```

The script will:

- create necessary directories
- set up git hooks
- symlink configurations to your home directory

## Structure

```
.
├── profile/    # Profile configurations
├── scripts/    # Utility scripts
├── terminal/   # Terminal-related configs
├── vim/        # Vim editor settings
└── ...         # Other tool configurations
```

## Customization

Create `.local` files for machine-specific settings:
- `~/.zshrc.local` for local shell settings
- `~/.vimrc.local` for local vim settings

## Updating

Pull the latest changes and the install script will run automatically:

```bash
git pull
```

let mapleader = ' '
" let maplocalleader = '\<'

" for jupyter notebook
set scrolloff=0

" general settings
set relativenumber
set number
set incsearch smartcase
set ignorecase
set clipboard+=ideaput
set clipboard+=unnamed
set clipboard+=unnamedplus

" set scrolloff=5
set showmode

" ideavim specific settings & emulated plugins
set ideajoin
set surround
set commentary
set idearefactormode=keep

" Use Tab and Shif+Tab to jump to Next/Previous Location
map <S-Tab> <C-O>

" general mappings
nnoremap Y y$
nnoremap Q @q

" searching
nnoremap s /
nnoremap <leader>fp :action FindInPath<CR>
nnoremap <leader>fu :action FindUsages<CR>
nnoremap <leader>fa :action GotoAction<CR>
nmap <leader>w :action SaveAll<CR>

" code navigation
nnoremap gi :action GotoImplementation<CR>
nnoremap gd :action GotoDeclaration<CR>
nnoremap gt :action GotoTest<CR>
nnoremap gr :action GotoRelated<CR>

" error navigation
nnoremap en :action GotoNextError<CR>
nnoremap gn :action GotoPreviousError<CR>
nnoremap <leader> gp :action VcsShowPrevChangeMarker<CR>
nnoremap <leader> gn :action VcsShowNextChangeMarker<CR>

" history jumping
nnoremap <C-o> :action Back<CR>
nnoremap <C-i> :action Forward<CR>

" vcs navigation
nnoremap <leader>gp :action VcsShowPrevChangeMarker<CR>
nnoremap <leader>gn :action VcsShowNextChangeMarker<CR>
nnoremap <leader>gb :action Annotate<CR>
nnoremap <leader>gh :action Vcs.ShowTabbedFileHistory<CR>
nnoremap <leader>gs :action ActivateCommitToolWindow<CR>
nnoremap <leader>gl :action ActivateVersionControlToolWindow<CR>

" refactoring
nnoremap <rn :action RenameElement<CR>
nnoremap <rf :action RenameFile<CR>
nnoremap <ff :action ReformatCode<CR>

" run configurations
nnoremap <leader>dd :action Debug<CR>
nnoremap <leader>dc :action ChooseDebugConfiguration<CR>
nnoremap <leader>rr :action Run<CR>
nnoremap <leader>rc :action ChooseRunConfiguration<CR>

" miscellaneous
nnoremap -          :action ActivateProjectToolWindow<CR>
nnoremap <esc>      :action HideAllWindows<CR>
nnoremap <leader>a  :action ShowIntentionActions<CR>
nnoremap <leader>G  :action ToggleDistractionFreeMode<CR>
nnoremap <leader>o  :action FileStructurePopup<CR>
nnoremap <leader>su :action ShowUsages<CR>
nnoremap <leader>cv :action ChangeView<CR>
nnoremap <leader>bb :action ToggleLineBreakpoint<CR>
nnoremap <leader>br :action ViewBreakpoints<CR>
nnoremap <leader>ic :action InspectCode<CR>
nnoremap <leader>oi :action OptimizeImports<CR>
nnoremap 				:action CommentByLineComment<CR>

set NERDTree
let g:NERDTreeMapActivateNode='l'
let g:NERDTreeMapJumpParent='h'

" navigation
nnoremap <leader>h  :action PreviousTab<CR>
nnoremap <leader>l  :action NextTab<CR>
nnoremap <leader>bd  :action CloseEditor<CR>
nnoremap <a-j> :action PreviousTab<CR>
nnoremap <a-k> :action NextTab<CR>

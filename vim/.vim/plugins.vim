call plug#begin('~/.vim/plugged')

Plug 'mhinz/vim-startify'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" toggle search highlighting when searching
Plug 'romainl/vim-cool'

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

if has('nvim') | let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6 } } | endif

Plug 'tpope/vim-fugitive'

Plug 'tpope/vim-repeat'

Plug 'szw/vim-maximizer'

" Language pack for vim
Plug 'sheerun/vim-polyglot'

" File icons within vim
Plug 'ryanoasis/vim-devicons'

Plug 'markonm/traces.vim'

" Nerdtree file explorer
Plug 'preservim/nerdtree'

Plug 'tpope/vim-surround'

" base16 colorschemes
" Plug 'chriskempson/base16-vim'
Plug 'tinted-theming/base16-vim'

" See tags from current file
Plug 'majutsushi/tagbar'

" Generating tags in projects (problem with generating huge tags)
" Plug 'ludovicchabant/vim-gutentags'

Plug 'Shougo/vimproc.vim', {'do' : 'make'}

" Snippets including community maintained ultisnips
Plug 'honza/vim-snippets'

Plug 'vim-syntastic/syntastic'

" Plug 'neoclide/coc.nvim', {'tag': 'v0.0.81'}
Plug 'neoclide/coc.nvim', {'branch': 'release'}


" Debugger extension

" Linter for different languages
Plug 'dense-analysis/ale'

" |---------------------|
" |    Markdown/Latex   |
" |---------------------|

Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' }

" Markdown support with folding and syntax highlighting
" Plug 'plasticboy/vim-markdown'

Plug 'andymass/vim-matchup'

Plug 'tpope/vim-commentary'

" |---------------------|
" |       UI            |
" |---------------------|

Plug 'junegunn/limelight.vim' " highlight current paragraph

Plug 'github/copilot.vim'

" display all buffers if only one tab is open
let g:airline#extensions#tabline#enabled = 1

let g:airline_powerline_fonts = 1

call plug#end()

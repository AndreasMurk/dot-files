" set space as leader key
let mapleader = " "

" config files management
map <silent> <leader>ev :edit ~/.vim/.vimrc <CR>
map <silent> <leader>em :edit ~/.vim/mappings.vim <CR>
map <silent> <leader>ep :edit ~/.vim/plugins.vim <CR>
map <silent> <leader>ez :edit ~/.config/zsh/.zshrc <CR>
map <silent> <leader>ea :edit ~/.config/aliasrc <CR>
map <silent> <leader>sv :w<CR> :source ~/.vim/.vimrc <CR>

" |---------------------|
" |       NAVIGATION    |
" |---------------------|

" <Tab> - next pos in jumplist
" <S-Tab> - old pos in jumplist
map <S-Tab> <C-O>

" save/quit quickly
nmap <silent> <leader>w :write!<CR>
nmap <silent> <leader>q :quit!<CR>
nmap <silent> <leader>d :bd!<CR>
nmap <silent> <leader>o :only<CR>

" shortcutting split navigation
nmap <C-h> <C-w>h
nmap <C-l> <C-w>l
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k

" creating splits
nmap <silent> <leader>- :vs<CR>
nmap <silent> <leader>_ :sp<CR>

" mapping for toggling theme
map <F1> :ToggleTheme<CR>

" |---------------------|
" |       NERDTREE      |
" |---------------------|

" NERDTree
nmap <C-n> :NERDTreeToggle<CR>

" close vim when only one window with nerdtree is opened
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" buffer navigation
nmap <silent> <leader>l :bn<CR>
nmap <silent> <leader>h :bp<CR>
nmap <silent> <leader>b :Buffers<CR>

" fzf
map <silent> <leader>F :FZF<CR>
map <silent> <leader>r :History<CR>
map <silent> <leader>S :Ag<CR>
map <silent> <leader>M :Maps<CR>

" markdown preview
let vim_markdown_preview_hotkey='<C-m>'

" compilation
map <leader>cf :CompileFile<CR>

" makefile compilation
map <silent> <F2> :make<CR>

" Formatting code
nmap <silent> <F4> <Plug>(coc-format)

" forwardSearch for latex
nmap <leader>fs :CocCommand latex.ForwardSearch<CR>

" ALE previous/next error
nmap <silent> <leader>k :ALEPrevious
nmap <silent> <leader>j :ALENext

" Git commands 
nmap <silent> <leader>gn <Plug>(coc-git-nextchunk)
nmap <silent> <leader>gp <Plug>(coc-git-prevchunk)
nmap <silent> <leader>gi <Plug>(coc-git-chunkinfo)

map <silent> <leader>R  :ShellHistory<CR>

" complete filepath
imap <c-x><c-f> <plug>(fzf-complete-path)

" complete command from shell history
command! -nargs=0 ShellHistory call feedkeys("i<c-x><c-R>", 't')
inoremap <expr> <c-x><c-R>
            \ fzf#vim#complete(fzf#wrap({
            \ 'source': "sed -e 's/: [0-9]*:0;//' -e '/^$/d' $HISTFILE" ,
            \ 'options': '--tac --no-sort',
            \ }))

" |---------------------|
" |       UI            |
" |---------------------|
" Plugin GOYO
nmap <leader>G :Goyo<CR>


" |---------------------|
" |       DEBUGGING     |
" |---------------------|
" Debugger remaps
"
nnoremap <leader>m :MaximizerToggle!<CR>
nnoremap <leader>dd :call vimspector#Launch()<CR>
nnoremap <leader>de :call vimspector#Reset()<CR>

nnoremap <leader>dc :call GotoWindow(g:vimspector_session_windows.code)<CR>
nnoremap <leader>dt :call GotoWindow(g:vimspector_session_windows.tagpage)<CR>
nnoremap <leader>dv :call GotoWindow(g:vimspector_session_windows.variables)<CR>
nnoremap <leader>dw :call GotoWindow(g:vimspector_session_windows.watches)<CR>
nnoremap <leader>ds :call GotoWindow(g:vimspector_session_windows.stack_trace)<CR>
nnoremap <leader>do :call GotoWindow(g:vimspector_session_windows.output)<CR>

nnoremap <leader>ab :call vimspector#ToggleBreakpoint()<CR>

nnoremap <leader>dtcb :call vimspector#CleanLineBreakpoint()<CR>
nnoremap <F7> :call vimspector#StepInto()<CR>
nnoremap <F8> :call vimspector#StepOut()<CR>

" yank file path
nnoremap <Leader>y :let @+=expand("%:~:.")<CR>:echo 'Yanked relative path'<CR>
nnoremap <Leader>Y :let @+=expand("%:p")<CR>:echo 'Yanked absolute path'<CR>

" auto install vundle plugin manager
if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
				\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

nmap zuz <Plug>(FastFoldUpdate)
let g:fastfold_savehook = 1
let g:fastfold_fold_command_suffixes =  ['x','X','a','A','o','O','c','C']
let g:fastfold_fold_movement_commands = [']z', '[z', 'zj', 'zk']

" ale
let g:ale_fixers = {
			\ '*': ['remove_trailing_lines', 'trim_whitespace'],
			\ 'json': ['remove_trailing_lines', 'trim_whitespace', 'fixjson'],
			\ 'python': ['yapf']
			\ }

let g:markdown_folding = 1
let g:tex_fold_enabled = 1
let g:vimsyn_folding = 'af'
let g:xml_syntax_folding = 1
let g:javaScript_fold = 1
let g:sh_fold_enabled= 7
let g:ruby_fold = 1
let g:perl_fold = 1
let g:perl_fold_blocks = 1
let g:r_syntax_folding = 1
let g:rust_fold = 1
let g:php_folding = 1

" set mouse
set mouse

" Set keymap to other key than <TAB>
let g:UltiSnipsExpandTrigger = '<F5>'

" Gutenberg settings
source ~/.vim/gutenberg.vim

" refresh preview on save markdow
let vim_markdown_preview_toggle=2

" Remove trailing whitespace on save
autocmd BufWritePre * %s/\s+$//e

" Fallback tex linter
let g:tex_flavor = 'latex'

" ALE airline support
let g:airline#extensions#ale#enabled = 1

" Disable linters and let ALE do the work
let g:ale_disable_lsp = 1

" VIM Plugins
source ~/.vim/plugins.vim

"VIM Mappings
source ~/.vim/mappings.vim

"VIM Functions
source ~/.vim/functions.vim

" |---------------------|
" |       OPTIONS       |
" |---------------------|

filetype plugin indent on
set wildmenu
set number
set relativenumber
set linebreak
syntax on
set noswapfile
set ignorecase
set showmatch
set hidden
set clipboard+=unnamedplus
set cursorline

" Update plugins when opening vim with startify
let g:startify_commands = [
            \   { 'upp': [ 'Update Plugins', ':PlugUpdate' ] },
            \   { 'ug': [ 'Upgrade Plugin Manager', ':PlugUpgrade' ] },
            \   { 'up': [ 'Update COC', ':CocUpdate' ] },
            \ ]


" coc global extension
let g:coc_global_extensions = [
			\ 'coc-pairs',
			\ 'coc-json',
			\ 'coc-sh',
			\ 'coc-git',
			\ 'coc-snippets',
			\ 'coc-vimlsp',
			\ 'coc-yaml',
			\ 'coc-docker',
			\ ]

" PHP
autocmd FileType php set omnifunc=phpcomplete#CompletePHP

set completeopt=noinsert,menuone,noselect

source ~/.vim/coc.vim

set termguicolors


if exists('$BASE16_THEME')
      \ && (!exists('g:colors_name') || g:colors_name != 'base16-$BASE16_THEME')
	colorscheme base16-$BASE16_THEME
endif

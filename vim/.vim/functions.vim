" toggle between dark and light theme
command! -nargs=0 ToggleTheme  call ToggleTheme()
command! -nargs=0 Light	colorscheme base16-atelier-heath-light | set background=light
command! -nargs=0 Dark	colorscheme base16-dracula 			| set background=dark

function GotoWindow(id)
    call win_gotoid(a:id)
    MaximizerToggle
endfunction

function! s:goyo_enter()
  set noshowmode
  set noshowcmd
endfunction

function! s:goyo_leave()
  set showmode
  set showcmd
endfunction

command! -nargs=* CompileFile :call Compile("%", <f-args>)
function Compile(file, ...) abort
	execute "w!"
	" compatible compilation
	execute has('nvim') ? "terminal" : "!clear &&"
				\ "compile.sh" expand(a:file) join(a:000)
endfunction

function HighlightTerminalCursor() abort
    " highlight groups for terminal cursor
    hi! TermCursorNC ctermfg=15 guifg=#fdf6e3 ctermbg=14 guibg=#93a1a1 cterm=NONE gui=NONE
endfunction

function UpdateTheme() abort
    " base16 theme setup
    if filereadable(expand("~/.vimrc_background"))
        let base16colorspace=256
        let theme = getenv('BASE16_THEME')
        if theme != v:null
            let g:airline_theme='base16_' . substitute(theme, "-", "_", "g")
        endif
        source ~/.vimrc_background
    endif
    call HighlightTerminalCursor()
endfunction

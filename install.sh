#!/usr/bin/env bash

[ -f .git/hooks/post-merge ] || {
    echo "Setup post-merge git hook ..."
    cat <<-EOF > .git/hooks/post-merge
	#!/bin/sh
	$0
	EOF
}

echo "Stowing dot-files ..."

# ensure some directories are not symlinked
mkdir -p "$HOME/.vim/autoload"
mkdir -p "$HOME/.local/bin"

# shellcheck disable=SC2046,SC2035
stow -v -t "$HOME" $(ls -d */)

# Stow only `extensions.json` inside `.windsurf/extensions`
echo "Stowing Windsurf settings..."
(
    cd .windsurf/extensions && stow -v -t "$HOME/.windsurf/extensions" .
    cd ../../ 
    cd .config/Windsurf/User && stow -v -t "$HOME/.config/Windsurf/User" .
)

#!/usr/bin/env bash

exists() { command -v "$1" > /dev/null; }

# zsh
alias szsh="source ~/.config/zsh/.zshrc"

# arbitrary
alias tf="tail -f"
alias ll="ls -la"
alias sal="alias | grep"

exists rg && {
	alias rg="rg -i"
}

exists docker compose && {
	alias dc="docker compose"	
  alias dcu="docker compose up"
	alias dcd="docker compose down"
	alias dcdv="docker compose down -v"
	alias dcb="docker compose build"
	alias dcl="docker compose logs"
	alias dce="docker compose exec -it"
  alias dps="docker compose ps"
}

exists eza && {
	alias l="eza --icons --long --all"
	alias ll="eza --icons --long --all --all"
	alias tree="eza --tree"
}

exists tmux && {
	alias etmuxconf="vim $HOME/.tmux/.tmux.conf.local"
	alias etmux="vim $HOME/.tmux/.tmux.conf.local"
	alias t="tmux -2"
	alias ntmx="tmux new-session -t"
	alias atmx="tmux attach-session -t"
	alias ktmx="tmux kill-session -t"
	alias katmx="tmux kill-session -a"
	alias tls="tmux ls"
	alias tmux="tmux -2"

}

exists git && {
	alias gbp='git branch --merged | grep -v "\*" | grep -vE "(main|develop|master)" | xargs -r -n 1 git branch -d'
  alias gstst='git stash -- $(git diff --staged --name-only)'
}

# applications
exists ranger && {
	alias r="ranger"
}

# vim as neovim 
exists nvim && {
	alias vim="nvim"
}

exists dragon-drop && {
	alias dr="dragon-drop"
}

exists pnpm && {
	alias p="pnpm"
	alias pi="pnpm install"
	alias pa="pnpm add"
	alias pu="pnpm uninstall"
	alias put="pnpm update"
	alias pd="pnpm dev"
	alias pb="pnpm build"
	alias px="pnpx"
	alias pl="pnpm lint"
	alias pf="pnpm format"
	
}

exists npm && {
	alias n="npm"
	alias ni="npm install"
	alias nu="npm uninstall"
	alias nup="npm update"
	alias nb="npm run build"
	alias nx="npx"
}

exists turbo && {
	alias tu="turbo"
}

# edit config files
alias evim="vim ~/.vim/.vimrc"
alias ezsh="vim ~/.config/zsh/.zshrc"
alias ealias="vim ~/.config/aliasrc"
alias eprof="vim ~/.profile"
alias zprof="vim ~/.zprofile"
alias zenv="vim ~/.zshenv"
alias etmx="vim ~/.tmux/.tmux.conf.local"
alias essh="vim ~/.ssh/config"

# basic aliases 
alias upt="sudo pacman -Syyy"
alias upg="sudo pacman -Syyyu && yay -Syu"
alias int="sudo pacman -S"
alias rem="sudo pacman -Rns"
alias list="sudo pacman -Q | grep"
alias se="sudo pacman -Ss"
alias yse="yay -Ss"
alias yint="yay -S"
alias yupg="yay -Syu"
alias yrem="yay -Rnu"
alias untar="tar -xzvf"
alias cat="bat"
alias rm="sudo rm -r"
alias xo="xdg-open"

# shutdown and reboot 
alias reb="sudo reboot now"
alias sud="sudo shutdown now"

# alias for gcc
alias compile="gcc -Wall -Werror -std=gnu11"

# alias for clipboard
alias copy="xclip -selection clipboard"

# systemctl
alias sc-status="sudo systemctl status"
alias sc-show="sudo systemctl show"
alias sc-start="sudo systemctl start"
alias sc-stop="sudo systemctl stop"
alias sc-restart="sudo systemctl restart"
alias sc-enable="sudo systemctl enable"
alias sc-disable="sudo systemctl disable"

# xournalpp
# alias xournalpp="wacom.sh && xournalpp"

# theme toggling
alias zts="theme_switch"
alias mux="tmuxinator"
